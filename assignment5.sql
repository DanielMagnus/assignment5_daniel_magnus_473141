-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 02. Nov, 2017 17:13 PM
-- Server-versjon: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `club`
--

CREATE TABLE `club` (
  `clubId` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `city` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `county` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `club`
--

INSERT INTO `club` (`clubId`, `name`, `city`, `county`) VALUES
('asker-ski', 'Asker skiklubb', 'Asker', 'Akershus'),
('lhmr-ski', 'Lillehammer Skiklub', 'Lillehammer', 'Oppland'),
('skiklubben', 'Trondhjems skiklub', 'Trondheim', 'Sør-Trøndelag'),
('vindil', 'Vind Idrettslag', 'Gjøvik', 'Oppland');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skier`
--

CREATE TABLE `skier` (
  `userName` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `fname` varchar(20) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `lname` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `yearOfBirth` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skier`
--

INSERT INTO `skier` (`userName`, `fname`, `lname`, `yearOfBirth`) VALUES
('ande_andr', 'Anders', 'Andresen', '2004'),
('ande_rønn', 'Anders', 'Rønning', '2001'),
('andr_stee', 'Andreas', 'Steen', '2001'),
('anna_næss', 'Anna', 'Næss', '2005'),
('arne_anto', 'Arne', 'Antonsen', '2005'),
('arne_inge', 'Arne', 'Ingebrigtsen', '2005'),
('astr_amun', 'Astrid', 'Amundsen', '2001'),
('astr_sven', 'Astrid', 'Svendsen', '2008'),
('bent_håla', 'Bente', 'Håland', '2009'),
('bent_svee', 'Bente', 'Sveen', '2003'),
('beri_hans', 'Berit', 'Hanssen', '2003'),
('bjør_ali', 'Bjørn', 'Ali', '2008'),
('bjør_rønn', 'Bjørg', 'Rønningen', '2009'),
('bjør_sand', 'Bjørn', 'Sandvik', '1997'),
('bjør_aase', 'Bjørn', 'Aasen', '2006'),
('bror_kals', 'Bror', 'Kalstad', '2006'),
('bror_﻿mos', 'Bror', '﻿Mostuen', '2005'),
('cami_erik', 'Camilla', 'Eriksen', '2005'),
('dani_hamm', 'Daniel', 'Hammer', '2000'),
('eina_nygå', 'Einar', 'Nygård', '2009'),
('elis_ruud', 'Elisabeth', 'Ruud', '2003'),
('elle_wiik', 'Ellen', 'Wiik', '2004'),
('erik_haal', 'Erik', 'Haaland', '2007'),
('erik_lien', 'Erik', 'Lien', '2008'),
('erik_pete', 'Erik', 'Petersen', '2002'),
('espe_egel', 'Espen', 'Egeland', '2005'),
('espe_haal', 'Espen', 'Haaland', '2004'),
('eva_kvam', 'Eva', 'Kvam', '2000'),
('fred_lien', 'Fredrik', 'Lien', '2000'),
('frod_mads', 'Frode', 'Madsen', '2008'),
('frod_rønn', 'Frode', 'Rønningen', '2005'),
('geir_birk', 'Geir', 'Birkeland', '2010'),
('geir_herm', 'Geir', 'Hermansen', '2003'),
('gerd_svee', 'Gerd', 'Sveen', '2001'),
('gunn_berg', 'Gunnar', 'Berge', '2009'),
('guri_nord', 'Guri', 'Nordli', '2003'),
('﻿hal_﻿mos', '﻿Halvor', '﻿Mostuen', '2009'),
('hann_stei', 'Hanno', 'Steiro', '2005'),
('hans_foss', 'Hans', 'Foss', '1998'),
('hans_løke', 'Hans', 'Løken', '2005'),
('hara_bakk', 'Harald', 'Bakken', '2002'),
('heid_dani', 'Heidi', 'Danielsen', '2005'),
('helg_brei', 'Helge', 'Breivik', '2006'),
('helg_toll', 'Helge', 'Tollefsen', '2003'),
('henr_bern', 'Henrik', 'Berntsen', '2003'),
('henr_dale', 'Henrik', 'Dalen', '2005'),
('henr_lore', 'Henrik', 'Lorentzen', '2006'),
('hild_hass', 'Hilde', 'Hassan', '2007'),
('håko_jens', 'Håkon', 'Jensen', '2005'),
('ida_mykl', 'Ida', 'Myklebust', '2001'),
('idar_kals', 'Idar', 'Kalstad', '2007'),
('idar_kals1', 'Idar', 'Kalstad', '2002'),
('inge_simo', 'Inger', 'Simonsen', '2004'),
('inge_thor', 'Inger', 'Thorsen', '2006'),
('ingr_edva', 'Ingrid', 'Edvardsen', '2001'),
('﻿jan_tang', '﻿Jan', 'Tangen', '2007'),
('juli_ande', 'Julie', 'Andersson', '2003'),
('kari_thor', 'Karin', 'Thorsen', '2002'),
('kjel_fjel', 'Kjell', 'Fjeld', '2004'),
('knut_bye', 'Knut', 'Bye', '2006'),
('kris_even', 'Kristian', 'Evensen', '2004'),
('kris_hass', 'Kristin', 'Hassan', '2003'),
('kris_hass1', 'Kristian', 'Hassan', '2004'),
('lind_lore', 'Linda', 'Lorentzen', '2004'),
('liv_khan', 'Liv', 'Khan', '2006'),
('magn_sand', 'Magnus', 'Sande', '2003'),
('mari_bye', 'Marit', 'Bye', '2003'),
('mari_dahl', 'Marit', 'Dahl', '2004'),
('mari_eile', 'Marius', 'Eilertsen', '2000'),
('mari_stra', 'Marius', 'Strand', '2005'),
('mart_halv', 'Martin', 'Halvorsen', '2002'),
('mona_lie', 'Mona', 'Lie', '2004'),
('mort_iver', 'Morten', 'Iversen', '2003'),
('nils_bakk', 'Nils', 'Bakke', '2003'),
('nils_knud', 'Nils', 'Knudsen', '2006'),
('odd_moha', 'Odd', 'Mohamed', '2005'),
('olav_bråt', 'Olav', 'Bråthen', '2000'),
('olav_eike', 'Olav', 'Eikeland', '2008'),
('olav_hell', 'Olav', 'Helle', '2007'),
('olav_lien', 'Olav', 'Lien', '2002'),
('ole_borg', 'Ole', 'Borge', '2002'),
('reid_hamr', 'Reidun', 'Hamre', '2008'),
('rolf_wiik', 'Rolf', 'Wiik', '2002'),
('rune_haga', 'Rune', 'Haga', '2005'),
('﻿rut_﻿mos', '﻿Ruth', '﻿Mostuen', '2002'),
('﻿rut_nord', '﻿Ruth', 'Nordli', '2006'),
('sara_okst', 'Sarah', 'Okstad', '2003'),
('silj_mykl', 'Silje', 'Myklebust', '2007'),
('sive_nord', 'Sivert', 'Nordli', '2009'),
('solv_solb', 'Solveig', 'Solbakken', '2004'),
('stia_andr', 'Stian', 'Andreassen', '2004'),
('stia_haug', 'Stian', 'Haugland', '2002'),
('stia_henr', 'Stian', 'Henriksen', '2001'),
('terj_mort', 'Terje', 'Mortensen', '2003'),
('thom_inge', 'Thomas', 'Ingebrigtsen', '2006'),
('tom_bråt', 'Tom', 'Bråthen', '2008'),
('tom_bøe', 'Tom', 'Bøe', '2008'),
('tom_jako', 'Tom', 'Jakobsen', '2002'),
('tor_dale', 'Tor', 'Dalen', '2005'),
('tore_gulb', 'Tore', 'Gulbrandsen', '2005'),
('tore_svee', 'Tore', 'Sveen', '2001'),
('tove_moe', 'Tove', 'Moe', '2002'),
('trin_kals', 'Trine', 'Kalstad', '2009'),
('tron_kris', 'Trond', 'Kristensen', '2006'),
('tron_moen', 'Trond', 'Moen', '2004'),
('øyst_lore', 'Øystein', 'Lorentzen', '2004'),
('øyst_sæth', 'Øystein', 'Sæther', '2000'),
('øyst_aase', 'Øystein', 'Aasen', '2007'),
('øyvi_hell', 'Øyvind', 'Helle', '2000'),
('øyvi_jens', 'Øyvind', 'Jenssen', '1999'),
('øyvi_kvam', 'Øyvind', 'Kvam', '2000'),
('øyvi_vike', 'Øyvind', 'Viken', '2004');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `skiseason`
--

CREATE TABLE `skiseason` (
  `seasonYear` year(4) NOT NULL,
  `totalDistance` int(11) DEFAULT NULL,
  `skierUserName` varchar(30) CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `skiClubId` varchar(50) CHARACTER SET utf8 COLLATE utf8_danish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `skiseason`
--

INSERT INTO `skiseason` (`seasonYear`, `totalDistance`, `skierUserName`, `skiClubId`) VALUES
(2015, 23, 'ande_andr', 'skiklubben'),
(2015, 942, 'ande_rønn', 'lhmr-ski'),
(2015, 440, 'andr_stee', 'asker-ski'),
(2015, 3, 'anna_næss', 'skiklubben'),
(2015, 32, 'arne_anto', 'skiklubben'),
(2015, 1, 'arne_inge', 'skiklubben'),
(2015, 961, 'astr_amun', 'lhmr-ski'),
(2015, 1, 'astr_sven', 'skiklubben'),
(2015, 19, 'bent_håla', 'asker-ski'),
(2015, 125, 'bent_svee', 'asker-ski'),
(2015, 448, 'beri_hans', 'asker-ski'),
(2015, 47, 'bjør_ali', 'asker-ski'),
(2015, 33, 'bjør_rønn', 'lhmr-ski'),
(2015, 460, 'bjør_sand', 'lhmr-ski'),
(2015, 121, 'bjør_aase', 'asker-ski'),
(2015, 1, 'cami_erik', 'vindil'),
(2015, 33, 'dani_hamm', 'lhmr-ski'),
(2015, 31, 'eina_nygå', 'lhmr-ski'),
(2015, 341, 'elis_ruud', 'asker-ski'),
(2015, 12, 'elle_wiik', 'lhmr-ski'),
(2015, 122, 'erik_haal', 'lhmr-ski'),
(2015, 1, 'erik_lien', 'vindil'),
(2015, 581, 'erik_pete', 'vindil'),
(2015, 519, 'espe_egel', 'skiklubben'),
(2015, 1, 'espe_haal', 'lhmr-ski'),
(2015, 28, 'eva_kvam', 'skiklubben'),
(2015, 113, 'fred_lien', 'asker-ski'),
(2015, 1, 'frod_mads', 'skiklubben'),
(2015, 237, 'frod_rønn', 'lhmr-ski'),
(2015, 69, 'geir_birk', 'skiklubben'),
(2015, 891, 'geir_herm', 'asker-ski'),
(2015, 173, 'gerd_svee', 'lhmr-ski'),
(2015, 2, 'gunn_berg', 'vindil'),
(2015, 240, 'hans_foss', 'asker-ski'),
(2015, 3, 'hans_løke', 'skiklubben'),
(2015, 7, 'hara_bakk', 'lhmr-ski'),
(2015, 3, 'heid_dani', 'asker-ski'),
(2015, 27, 'helg_brei', 'skiklubben'),
(2015, 9, 'helg_toll', 'skiklubben'),
(2015, 799, 'henr_bern', 'asker-ski'),
(2015, 2, 'henr_dale', NULL),
(2015, 1, 'henr_lore', 'vindil'),
(2015, 2, 'hild_hass', 'lhmr-ski'),
(2015, 778, 'håko_jens', 'lhmr-ski'),
(2015, 666, 'ida_mykl', 'skiklubben'),
(2015, 3, 'inge_simo', 'asker-ski'),
(2015, 194, 'inge_thor', NULL),
(2015, 294, 'ingr_edva', 'skiklubben'),
(2015, 2, '﻿jan_tang', NULL),
(2015, 20, 'juli_ande', 'skiklubben'),
(2015, 261, 'kari_thor', 'skiklubben'),
(2015, 1, 'kjel_fjel', 'asker-ski'),
(2015, 2, 'knut_bye', 'lhmr-ski'),
(2015, 586, 'kris_even', 'skiklubben'),
(2015, 4, 'kris_hass', 'skiklubben'),
(2015, 391, 'kris_hass1', 'skiklubben'),
(2015, 578, 'lind_lore', 'lhmr-ski'),
(2015, 178, 'liv_khan', 'asker-ski'),
(2015, 200, 'magn_sand', 'asker-ski'),
(2015, 362, 'mari_bye', NULL),
(2015, 576, 'mari_dahl', 'lhmr-ski'),
(2015, 18, 'mari_eile', 'lhmr-ski'),
(2015, 41, 'mari_stra', 'skiklubben'),
(2015, 63, 'mart_halv', 'vindil'),
(2015, 7, 'mona_lie', 'skiklubben'),
(2015, 2, 'mort_iver', 'skiklubben'),
(2015, 36, 'nils_bakk', 'lhmr-ski'),
(2015, 4, 'nils_knud', 'skiklubben'),
(2015, 352, 'odd_moha', 'skiklubben'),
(2015, 17, 'olav_bråt', NULL),
(2015, 2, 'olav_eike', 'lhmr-ski'),
(2015, 1, 'olav_hell', 'skiklubben'),
(2015, 408, 'olav_lien', 'asker-ski'),
(2015, 311, 'ole_borg', 'lhmr-ski'),
(2015, 2, 'reid_hamr', 'skiklubben'),
(2015, 749, 'rolf_wiik', 'skiklubben'),
(2015, 228, 'rune_haga', 'asker-ski'),
(2015, 1, 'silj_mykl', 'asker-ski'),
(2015, 2, 'solv_solb', NULL),
(2015, 8, 'stia_andr', NULL),
(2015, 412, 'stia_haug', 'skiklubben'),
(2015, 62, 'stia_henr', 'vindil'),
(2015, 119, 'terj_mort', 'skiklubben'),
(2015, 15, 'thom_inge', 'vindil'),
(2015, 1, 'tom_bråt', 'vindil'),
(2015, 176, 'tom_bøe', 'vindil'),
(2015, 18, 'tom_jako', 'asker-ski'),
(2015, 408, 'tor_dale', 'skiklubben'),
(2015, 375, 'tore_gulb', 'lhmr-ski'),
(2015, 1156, 'tore_svee', 'skiklubben'),
(2015, 321, 'tove_moe', 'asker-ski'),
(2015, 3, 'tron_kris', 'skiklubben'),
(2015, 8, 'tron_moen', 'vindil'),
(2015, 13, 'øyst_lore', 'skiklubben'),
(2015, 831, 'øyst_sæth', 'vindil'),
(2015, 2, 'øyst_aase', 'skiklubben'),
(2015, 950, 'øyvi_hell', 'asker-ski'),
(2015, 3, 'øyvi_jens', 'skiklubben'),
(2015, 18, 'øyvi_kvam', 'asker-ski'),
(2015, 20, 'øyvi_vike', 'asker-ski'),
(2016, 55, 'ande_andr', 'skiklubben'),
(2016, 379, 'andr_stee', 'asker-ski'),
(2016, 3, 'anna_næss', 'skiklubben'),
(2016, 99, 'arne_anto', 'skiklubben'),
(2016, 2, 'arne_inge', 'skiklubben'),
(2016, 761, 'astr_amun', 'lhmr-ski'),
(2016, 3, 'astr_sven', 'skiklubben'),
(2016, 62, 'bent_håla', 'skiklubben'),
(2016, 374, 'beri_hans', 'asker-ski'),
(2016, 47, 'bjør_ali', 'asker-ski'),
(2016, 56, 'bjør_rønn', 'lhmr-ski'),
(2016, 449, 'bjør_sand', 'lhmr-ski'),
(2016, 116, 'bjør_aase', 'asker-ski'),
(2016, 202, 'bror_kals', NULL),
(2016, 243, 'bror_﻿mos', 'skiklubben'),
(2016, 1, 'cami_erik', 'vindil'),
(2016, 61, 'dani_hamm', 'lhmr-ski'),
(2016, 68, 'eina_nygå', 'skiklubben'),
(2016, 368, 'elis_ruud', 'skiklubben'),
(2016, 35, 'elle_wiik', 'lhmr-ski'),
(2016, 143, 'erik_haal', 'lhmr-ski'),
(2016, 556, 'espe_egel', 'skiklubben'),
(2016, 2, 'espe_haal', 'lhmr-ski'),
(2016, 89, 'eva_kvam', 'skiklubben'),
(2016, 122, 'fred_lien', 'asker-ski'),
(2016, 2, 'frod_mads', 'skiklubben'),
(2016, 71, 'geir_birk', 'skiklubben'),
(2016, 789, 'geir_herm', 'skiklubben'),
(2016, 196, 'gerd_svee', 'lhmr-ski'),
(2016, 2, 'gunn_berg', 'vindil'),
(2016, 17, 'guri_nord', 'skiklubben'),
(2016, 3, '﻿hal_﻿mos', 'asker-ski'),
(2016, 14, 'hann_stei', 'lhmr-ski'),
(2016, 276, 'hans_foss', 'lhmr-ski'),
(2016, 1, 'hans_løke', 'skiklubben'),
(2016, 16, 'hara_bakk', 'lhmr-ski'),
(2016, 3, 'heid_dani', 'asker-ski'),
(2016, 74, 'helg_brei', 'skiklubben'),
(2016, 2, 'henr_dale', NULL),
(2016, 1, 'henr_lore', 'vindil'),
(2016, 1, 'hild_hass', 'lhmr-ski'),
(2016, 804, 'håko_jens', 'lhmr-ski'),
(2016, 614, 'ida_mykl', 'skiklubben'),
(2016, 101, 'idar_kals', 'skiklubben'),
(2016, 1308, 'idar_kals1', 'vindil'),
(2016, 2, 'inge_simo', 'asker-ski'),
(2016, 220, 'inge_thor', NULL),
(2016, 309, 'ingr_edva', 'skiklubben'),
(2016, 4, '﻿jan_tang', NULL),
(2016, 34, 'juli_ande', 'skiklubben'),
(2016, 233, 'kari_thor', 'skiklubben'),
(2016, 2, 'kjel_fjel', 'skiklubben'),
(2016, 11, 'kris_hass', 'skiklubben'),
(2016, 334, 'kris_hass1', 'lhmr-ski'),
(2016, 551, 'lind_lore', 'lhmr-ski'),
(2016, 183, 'liv_khan', 'asker-ski'),
(2016, 166, 'magn_sand', 'asker-ski'),
(2016, 492, 'mari_dahl', 'lhmr-ski'),
(2016, 18, 'mari_eile', 'lhmr-ski'),
(2016, 35, 'mari_stra', 'skiklubben'),
(2016, 50, 'mart_halv', 'vindil'),
(2016, 12, 'mona_lie', 'skiklubben'),
(2016, 4, 'mort_iver', 'skiklubben'),
(2016, 93, 'nils_bakk', 'lhmr-ski'),
(2016, 2, 'nils_knud', 'skiklubben'),
(2016, 19, 'olav_bråt', NULL),
(2016, 2, 'olav_eike', 'lhmr-ski'),
(2016, 423, 'olav_lien', 'asker-ski'),
(2016, 314, 'ole_borg', 'lhmr-ski'),
(2016, 3, 'reid_hamr', 'skiklubben'),
(2016, 632, 'rolf_wiik', 'skiklubben'),
(2016, 248, 'rune_haga', 'asker-ski'),
(2016, 1237, '﻿rut_﻿mos', 'vindil'),
(2016, 368, '﻿rut_nord', 'skiklubben'),
(2016, 5, 'sara_okst', 'asker-ski'),
(2016, 2, 'silj_mykl', 'asker-ski'),
(2016, 1, 'sive_nord', 'skiklubben'),
(2016, 1, 'solv_solb', 'asker-ski'),
(2016, 9, 'stia_andr', 'vindil'),
(2016, 443, 'stia_haug', 'skiklubben'),
(2016, 49, 'stia_henr', 'vindil'),
(2016, 95, 'terj_mort', 'skiklubben'),
(2016, 26, 'thom_inge', 'vindil'),
(2016, 1, 'tom_bråt', 'vindil'),
(2016, 194, 'tom_bøe', 'vindil'),
(2016, 33, 'tom_jako', 'skiklubben'),
(2016, 342, 'tore_gulb', 'lhmr-ski'),
(2016, 352, 'tove_moe', 'asker-ski'),
(2016, 22, 'trin_kals', 'lhmr-ski'),
(2016, 5, 'tron_kris', 'skiklubben'),
(2016, 17, 'tron_moen', 'vindil'),
(2016, 47, 'øyst_lore', 'skiklubben'),
(2016, 631, 'øyst_sæth', 'vindil'),
(2016, 1, 'øyst_aase', 'skiklubben'),
(2016, 869, 'øyvi_hell', 'asker-ski'),
(2016, 2, 'øyvi_jens', 'skiklubben'),
(2016, 52, 'øyvi_vike', 'asker-ski');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`clubId`);

--
-- Indexes for table `skier`
--
ALTER TABLE `skier`
  ADD PRIMARY KEY (`userName`);

--
-- Indexes for table `skiseason`
--
ALTER TABLE `skiseason`
  ADD PRIMARY KEY (`seasonYear`,`skierUserName`),
  ADD KEY `skierUserName` (`skierUserName`),
  ADD KEY `skiClubId` (`skiClubId`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `skiseason`
--
ALTER TABLE `skiseason`
  ADD CONSTRAINT `skiseason_ibfk_1` FOREIGN KEY (`skierUserName`) REFERENCES `skier` (`userName`),
  ADD CONSTRAINT `skiseason_ibfk_2` FOREIGN KEY (`skiClubId`) REFERENCES `club` (`clubId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
