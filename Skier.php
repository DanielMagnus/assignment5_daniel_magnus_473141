<?php

class Skier {
	public $userName;
	public $fname;
	public $lname;
	public $yearOfBirth;

	public function __construct($userName, $fname, $lname, $yearOfBirth)  
    {  
        $this->userName = $userName;
        $this->fname = $fname;
	    $this->lname = $lname;
	    $this->yearOfBirth = $yearOfBirth;
    } 
}

?>