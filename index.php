<?php
include_once("DBModelTmpl.php");
include_once("DOMmodel.php");

$DBController = new DBModel();
if(!$DBController){
	echo "Kunne ikke koble til databasen\n";
}else{
	echo "Koblet til databasen\n";
	$doc = new DOMDocument();
	if (!$doc->load("SkierLogs.xml")) {
		echo "Klarte ikke laste xml fil";
	} else {
		echo "Klarte å laste xml fil";
		addSkiers($DBController, $doc);
		addClub($DBController, $doc);
		addSkiSeason($DBController, $doc);
	}

}

?>