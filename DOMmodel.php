<?php

include_once("Skier.php");
include_once("Club.php");
include_once("SkiSeason.php");

function addSkiers($DBController, $doc){
	$xpath = new DOMXpath($doc);
	foreach($xpath->query("/SkierLogs/Skiers/Skier") as $skier){	//get all Skier elements
		$children = $skier->childNodes;								//finds the children of Skier
		
		$userName = $skier->getAttribute('userName');				//get Skier name
		$fname = $children->item(1)->nodeValue;						//get fname
		$lname = $children->item(3)->nodeValue;						//get lnam
		$yearOfBirth = $children->item(5)->nodeValue;				//get yearOfBirth
		
		$DBController->addSkier(new skier($userName, $fname, $lname, $yearOfBirth));		//create skier object
	}
}

function addClub($DBController, $doc){
	$xpath = new DOMXpath($doc);
	foreach($xpath->query("/SkierLogs/Clubs/Club") as $club){	//get all club elements
		$children = $club->childNodes;							//finds the children of Club
		
		$clubId = $club->getAttribute('id');					//get Club id
		$name = $children->item(1)->nodeValue;					//get name
		$city = $children->item(3)->nodeValue;					//get city
		$county = $children->item(5)->nodeValue;				//get county
		
		$DBController->addClub(new club($clubId, $name, $city, $county));	//create club object
	}
}

function addSkiSeason($DBController, $doc){
	$xpath = new DOMXpath($doc);
	
	foreach($xpath->query("/SkierLogs/Season") as $season){		//get all seasons
		$seasonYear = $season->getAttribute('fallYear');		//set variable to fallYear
		echo "\n" . $seasonYear;
		foreach($xpath->query("/SkierLogs/Season[@fallYear = $seasonYear]/Skiers") as $season1){	//for each season find skiers
			if(!$season1->hasAttribute('clubId')){		//if Skiers have no clubId
				$skiClubId = NULL;
			foreach($xpath->query("/SkierLogs/Season[@fallYear = $seasonYear]/Skiers[not(@*)]/Skier") as $season2){		//finds all Skiers wihtout club
				$skierUserName = $season2->getAttribute('userName');			//get username
				echo $skierUserName . "\n\t";
				$totalDistance = 0;			//initialize/reset totaldistance for skier
				foreach($xpath->query("/SkierLogs/Season[@fallYear = $seasonYear]/Skiers[not(@*)]/Skier[@userName = \"$skierUserName\"]/Log/Entry/Distance")as $season3){
					$totalDistance += $season3->nodeValue;						//finds distance for skier and adds to total distance
				}
				$DBController->addSeason(new SkiSeason($seasonYear, $totalDistance, $skierUserName, $skiClubId));		//creates season object
				echo $totalDistance . "\n\t";
				
			}
			}else{			//if skiers have clubId
			$skiClubId = $season1->getAttribute('clubId');			//get club id
			echo $skiClubId . "\n\n\t";
			foreach($xpath->query("/SkierLogs/Season[@fallYear = $seasonYear]/Skiers[@clubId = \"$skiClubId\"]/Skier") as $season2){	//finds all skiers within a club
				$skierUserName = $season2->getAttribute('userName');			//get userName
				echo $skierUserName . "\n\t";
				$totalDistance = 0;				//initialize/reset totalDistance
				foreach($xpath->query("/SkierLogs/Season[@fallYear = $seasonYear]/Skiers[@clubId = \"$skiClubId\"]/Skier[@userName = \"$skierUserName\"]/Log/Entry/Distance")as $season3){
					$totalDistance += $season3->nodeValue;				//find distance of skiers with club id
				}
				$DBController->addSeason(new SkiSeason($seasonYear, $totalDistance, $skierUserName, $skiClubId));		//create season object
				echo $totalDistance . "\n\t";
				
			}
			}
		
		}
		
		
	}
}

?>