<?php
include_once("TestDBProps.php");


class DBModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
			$this->db = new PDO('mysql:host='.TEST_DB_HOST.'; dbname=assignment5; charset=utf8', 
			TEST_DB_USER, TEST_DB_PWD);
			$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
    }

    public function addSkier($skier)
    {

		try{
		$stmt = $this->db->prepare("INSERT INTO skier(userName, fname, lname, yearOfBirth)
									VALUES(:userName, :fname, :lname, :yearOfBirth)");
		$stmt->bindValue(':userName', $skier->userName, PDO::PARAM_STR);
		$stmt->bindValue(':fname', $skier->fname, PDO::PARAM_STR);
		$stmt->bindValue(':lname', $skier->lname, PDO::PARAM_STR);
		$stmt->bindValue(':yearOfBirth', $skier->yearOfBirth, PDO::PARAM_STR);

			$stmt->execute();
		
		}
	catch(PDOException $e){
		print_r($e->getMessage());
		}
		
	}
	
	    public function addClub($club)
    {

		try{
		$stmt = $this->db->prepare("INSERT INTO club(clubId, name, city, county)
									VALUES(:club, :name, :city, :county)");
		$stmt->bindValue(':club', $club->clubId, PDO::PARAM_STR);
		$stmt->bindValue(':name', $club->name, PDO::PARAM_STR);
		$stmt->bindValue(':city', $club->city, PDO::PARAM_STR);
		$stmt->bindValue(':county', $club->county, PDO::PARAM_STR);

			$stmt->execute();
		
		}
	catch(PDOException $e){
			print_r($e->getMessage());
		}
		
	}
	
	public function addSeason($season)
    {

		try{
		$stmt = $this->db->prepare("INSERT INTO skiseason(seasonYear, totalDistance, skierUserName, skiClubId)
									VALUES(:seasonYear, :totalDistance, :skierUserName, :skiClubId)");
		$stmt->bindValue(':seasonYear', $season->seasonYear, PDO::PARAM_STR);
		$stmt->bindValue(':totalDistance', $season->totalDistance, PDO::PARAM_STR);
		$stmt->bindValue(':skierUserName', $season->skierUserName, PDO::PARAM_STR);
		$stmt->bindValue(':skiClubId', $season->skiClubId, PDO::PARAM_STR);
		
			$stmt->execute();
		}
	catch(PDOException $e){
			print_r($e->getMessage());
		}
		
	}
}
?>