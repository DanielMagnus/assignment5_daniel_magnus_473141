<?php

class Club {
	public $clubId;
	public $name;
	public $city;
	public $county;

	public function __construct($clubId, $name, $city, $county)  
    {  
        $this->clubId = $clubId;
        $this->name = $name;
	    $this->city = $city;
	    $this->county = $county;
    } 
}

?>