<?php

class SkiSeason {
	public $seasonYear;
	public $totalDistance;
	public $skierUserName;
	public $skiClubId;

	public function __construct($seasonYear, $totalDistance, $skierUserName, $skiClubId)  
    {  
        $this->seasonYear = $seasonYear;
        $this->totalDistance = $totalDistance;
	    $this->skierUserName = $skierUserName;
	    $this->skiClubId = $skiClubId;
    } 
}

?>